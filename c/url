/* Copyright 2001 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* url.c */

#include <stdbool.h>
#include <stdlib.h>
#include "Interface/URL.h"
#include "swis.h"
#include "ink.h"
#include "url.h"

#define CONNECTED   (1<<0)
#define REQUESTED   (1<<1)
#define DATA_SENT   (1<<2)
#define REPLIED     (1<<3)
#define ONGOING     (1<<4)
#define COMPLETE    (1<<5)
#define ABORTED     (1<<6)

#define BUFF_SIZE   (1024)

static int  regDump[16];
static bool callBackClaimed = false;
static bool callAfterClaimed = false;
static bool sessionActive = false;
static int  session;

static _kernel_oserror *process(_kernel_swi_regs *r, void *pw)
{
  _kernel_oserror *err;
  char *buffer;
  int status;

  err = _swix( URL_Status, _INR(0,1) | _OUT(0), 0, session, &status );
  if( err ) return err;

  if( status & ( COMPLETE | ABORTED ) )
  {
    return url_finalise();
  }

  if( status & ONGOING )
  {
    buffer = malloc( BUFF_SIZE );
    if( buffer == NULL ) return NULL;
    while( status & ( COMPLETE | ABORTED ) == 0 )
    {
      int bytesRead = 1;
      while( !err && bytesRead > 0 )
      {
        err = _swix( URL_ReadData, _INR(0,3) | _OUT(0) | _OUT(4), 0,
                     session, buffer, BUFF_SIZE,
                     &status, &bytesRead );
      }
    }
  }

  return err;
}

/* url_fetch()
  Entry:
    url - The url to fetch, including the protocol, ie http
    extra - extra data to send as part of initial request
  Exit:
    success will be updated some time after this function returns
  Returns:
    Error
*/
_kernel_oserror *url_fetch( const char *url, const char *extra, bool *success )
{
  int status;

  _kernel_oserror *err = _swix( URL_Register, _IN(0) | _OUT(1), 0, &session );
  if( err ) return err;
  sessionActive = true;

  /* Start the fetch */
  err = _swix( URL_GetURL, _INR(0,6) | _OUT(0), 0, session, 2, url, INK_EXTRA_DATA, 2, NULL, &status );

  return err;
}

/* url_finalise()
  Tidy up any outstanding callEvery, callBack etc
*/
_kernel_oserror *url_finalise( void )
{
  int status;

  if( sessionActive ) _swix( URL_Deregister, _INR(0,1) | _OUT(0), 0, session, &status );
  sessionActive = false;

  return NULL;
}

/* End of url.c */
