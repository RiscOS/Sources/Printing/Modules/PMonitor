/* Copyright 2001 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* IdString.h */

#ifndef _IdString_h
#define _IdString.h

#include <stdlib.h>

#include "kernel.h"

/* This codes obtains the id string from parallel:                   */

/* Reads the ID string of the current printer and 0 terminates it    */
/* Entry:                                                            */
/*   string. Ptr to assign to.                                       */
/* Exit:                                                             */
/*   Ptr to string updated. Memory from malloc()                     */
/* Returns:                                                          */
/*   Error state                                                     */
_kernel_oserror *IdString_read( char **string );

/* Extracts a value from the IdString                                */
/* Entry:                                                            */
/*   idString        The string as read by IdString_read             */
/*   token0          The token to look up                            */
/*   token1          Alternative token. eg abreviated version        */
/* Returns:                                                          */
/*   ptr to token (memory from malloc) or NULL if not found          */

char *IdString_token( const char *idString, char *token0, char *token1 );

/* Tokens required by IEEE 1284-2000 */
#define IDSTRING_MDL              "MDL"
#define IDSTRING_MDL_L            "MODEL"
#define IDSTRING_MFG              "MFG"
#define IDSTRING_MFG_L            "MANUFACTURER"
#define IDSTRING_CMD              "CMD"
#define IDSTRING_CMD_L            "COMMAND SET"

/* HP specific tokens */
#define IDSTRING_HP_CLASS         "CLASS"
#define IDSTRING_HP_SERN          "SERN"
#define IDSTRING_HP_STATUS        "VSTATUS"

/* HP specific values */
#define IDSTRING_HP_MFG           "HEWLETT-PACKARD"
#define IDSTRING_HP_CLASS_PRINTER "PRINTER"
#define IDSTRING_HP_MDL_K80       "OfficeJet  K80"
#endif

/* End of IdString.h */
